class Array
  DEFAULT_THREADS_COUNT = 4

  # <editor-fold desc="Многопоточные аналоги операций">
  def parallel_all?(&predicate)
    parallel_execute { |chunk| chunk.all?(&predicate) }.all?
  end

  def parallel_any?(&predicate)
    parallel_execute { |chunk| chunk.any?(&predicate) }.any?
  end

  def parallel_select(&predicate)
    parallel_execute { |chunk| chunk.select(&predicate) }.flatten
  end

  def parallel_map(&function)
    parallel_execute { |chunk| chunk.map(&function) }.flatten
  end

  # </editor-fold>

  def parallel_execute
    threads_count = [DEFAULT_THREADS_COUNT, size].min

    (0...threads_count)
      .map { |thread_id| cut_chunk(thread_id, threads_count) }
      .map { |chunk| Thread.new { Thread.current['result'] = yield chunk } }
      .each(&:join)
      .map { |thread| thread['result'] }
  end

  def cut_chunk(chunk_id, chunks_count)
    from = chunk_id * (size / chunks_count) + [chunk_id, size % chunks_count].min
    chunk_size = size / chunks_count + (chunk_id < size % chunks_count ? 1 : 0)
    self[from, chunk_size]
  end
end


values = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

puts "#{values.select { |i| i % 2 == 0 }}"
puts "#{values.parallel_select { |i| i % 2 == 0 }}"

puts "#{values.map { |i| i * i }}"
puts "#{values.parallel_map { |i| i * i }}"