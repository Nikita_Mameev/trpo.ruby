# <editor-fold desc="Разбор входных параметров">
class Params
  attr_accessor :size, :characters

  def initialize(size, characters)
    @size = size
    @characters = characters
  end
end

def positive_integer?(str)
  str =~ /^[1-9]\d*$/
end

def array_of_characters?(arr)
  arr.find { |x| x.length > 1 }.nil?
end

def parse_params(args)
  if args.length < 2 ||
     !positive_integer?(args[0]) ||
     !array_of_characters?(args[1..args.length])
    puts 'Input must be: {positive integer - length of string} {1 or more symbols separated by spaces}'
    exit
  end

  Params.new(args[0].to_i, args[1..args.length])
end

# </editor-fold>

# <editor-fold desc="Генерация строк">
def generate_strings(params)
  strings = ['']

  1.upto(params.size) do |_i|
    strings = strings
              .map { |prefix|
                params.characters
                      .reject { |char| prefix[-1] == char }
                      .map { |char| prefix + char }
              }
              .flatten
  end

  strings
end
# </editor-fold>

params = parse_params(ARGV)
result = generate_strings(params)
puts result.to_s
