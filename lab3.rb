require 'date'

class ReportParser
  RECORD_REGEX = /^.*Дата\sсовершения\sоперации:\s(\d{2}\.\d{2}\.\d{4})# Дата
\s(\d{2}:\d{2})# Время
.*Терминал\sID:\s+[0-9]+\\[A-Z]+\\([A-Za-z]+)# Город
\\([\w .]*)# Адрес
\\(?:(.*)\\|)# Место совершения операции
.*Сумма\s(?:транзакции|авторизации):\s(\d+(?:\.\d*|))# Сумма (может быть с копейками)
.*$/x

  def parse(file_name)
    File.open(file_name, 'r') do |file|
      file.readlines.map { |line| parse_line(line) }
          .reject(&:nil?)
    end
  end

  def parse_line(line)
    matches = line.match(RECORD_REGEX)

    if matches.nil?
      nil
    else
      {
          date: Date.strptime(matches.captures[0], '%d.%m.%y'),
          time: matches.captures[1],
          city: matches.captures[2],
          address: matches.captures[3],
          organization: matches.captures[4],
          sum: matches.captures[5].to_f
      }
    end
  end
end

records = ReportParser.new.parse('data/data.lab3.txt')
